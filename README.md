# Vagrant WSL on Windows 10

Configure vagrant on WSL running on Windows 10

# Remount Windows drives on WSL with proper permissions
umount /mnt/d
mount -t drvfs D: /mnt/d -o metadata,umask=22,fmask=11
umount /mnt/c
mount -t drvfs C: /mnt/c -o metadata,umask=22,fmask=11

# Add VirtualBox, WindowsPowerShell and system32 paths to PATH 

1. VirtualBox provider - Point Vagrant to interact with VirtualBox installed on the Windows system, not within the WSL
2. Powershell - Force WSL to use Windows Powershell
3. PATH - Add Windows system32 path to WSL PATH


```
ln -s /mnt/c/Windows/System32/WindowsPowerShell/v1.0/powershell.exe /usr/local/bin/powershell
export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"
export PATH="$PATH:/mnt/c/Windows/System32/WindowsPowerShell/v1.0"
export PATH="$PATH:/mnt/c/WINDOWS/system32"
```

# Configure Windows Access
Vagrant will need access to the actual Windows system to function correctly.
To enable this access, run:

`export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"`

To access projects outside WSL (for example on a Windows system path D:\DevOpsProjects\VagrantProjects\Machines)

`export VAGRANT_WSL_WINDOWS_ACCESS_USER_HOME_PATH="/mnt/d/DevOpsProjects/VagrantProjects/Machines"`


# Vagrant file configuration
To avoid errors described [here](https://github.com/hashicorp/vagrant/issues/8604):


```
config.vm.provider "virtualbox" do |vb|
vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
end
```


OR

```
config.vm.provider "virtualbox" do |vb|
vb.customize ["modifyvm", :id, "--cableconnected1", "off"]
end
```

The full config file would look something like:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial32"
  config.vm.provider "virtualbox" do |vb|
    vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
  end  
end
```

You can then run `vagrant up` from your WSL 

